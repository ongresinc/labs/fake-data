module gitlab.com/ongresinc/fake-postgres

go 1.13

require (
	github.com/brianvoe/gofakeit v3.18.0+incompatible
	github.com/brianvoe/gofakeit/v5 v5.2.0
	github.com/dustin/go-humanize v1.0.0
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/go-pg/pg/v9 v9.1.6
	github.com/gocarina/gocsv v0.0.0-20200330101823-46266ca37bd3
	github.com/google/uuid v1.1.1
	github.com/spf13/cobra v1.0.0 // indirect
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.6.3
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d
)
