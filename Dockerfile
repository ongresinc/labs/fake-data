FROM registry.access.redhat.com/ubi8/ubi-minimal
COPY fake-data /usr/local/bin/fake-data
CMD ["/usr/local/bin/fake-data"]