# fake-data

> This is a work in progress! :D

This repo contains a Go script that generates fake data on a postgres server.

## how it works

This program will connect on a database, **REMOVE ALL OBJECTS** of the `fake_data` schema and create a table with random data, generated with the library [gofakeit](https://github.com/brianvoe/gofakeit). Once the table is created it will create the primary keys and "clone the table" as many times defined on the `maxCopies` parameter.

## dependencies

```
go version go1.13.6 darwin/amd64
```

## usage

```
fake-data generates fake data on your PostgreSQL database.

Usage:
  fake-data [OPTIONS]...

General Options:
  -d, --dbname string               database name to connect to (default "$USER")
      --debug                       show SQL code
  -h, --host string                 database server host or socket directory (default "local socket")
      --inserts                     create data with INSERT commands, rather than COPY
      --jobs int                    parallel jobs (default 10)
      --maintenanceWorkMem string   default value for 'maintenance_work_mem' GUC (default "2GB")
      --maxCopies int               number of copies of the original table (default 10)
      --maxRows int                 number of Rows per transaction (default 100)
  -W, --password                    force password prompt
  -p, --port int                    database server port (default 5432)
      --scale int                   Scale factor (default 100)
      --targetSchema string         default schema to create the tables (default "fake_data")
  -U, --username string             database user name (default "$USER")
  -V, --version                     output version information, then exit

Report BUGs on https://gitlab.com/ongresinc/labs/fake-data.
```