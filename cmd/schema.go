package main

import (
	"fmt"
	"log"
	"time"

	pg "github.com/go-pg/pg/v9"
	orm "github.com/go-pg/pg/v9/orm"
)

type User struct {
	ID       string `pg:",nopk,type:uuid"`
	Name     string
	Email    string
	BirthDay time.Time
	Salary   float64
	Phrase   string
	Company  string
	Title    string
}

func createSchema(db *pg.DB) error {

	log.Printf("Cleaning up the '%s' schema...", targetSchema)

	steps := []string{
		fmt.Sprintf("DROP SCHEMA IF EXISTS %s CASCADE;", targetSchema),
		fmt.Sprintf("CREATE SCHEMA %s;", targetSchema),
	}

	for _, sql := range steps {

		_, err := db.Exec(sql)

		if err != nil {
			panic(err)
		}
	}

	log.Println("Creating the tables...")
	for _, model := range []interface{}{(*User)(nil)} {

		err := db.CreateTable(model, &orm.CreateTableOptions{})
		if err != nil {
			return err
		}
	}
	return nil
}
