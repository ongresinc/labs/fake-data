package main

import (
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/dustin/go-humanize"
	pg "github.com/go-pg/pg/v9"
)

func createCopy(i int, db *pg.DB, wg *sync.WaitGroup, cmdList []string, startTime time.Time) {

	logf := func(format string, v ...interface{}) {
		args := make([]interface{}, 0)
		args = append(args, i, maxCopies, (i * 100 / maxCopies))
		args = append(args, v...)
		log.Printf("[job %05d/%d (%03d%%)] "+format, args...)
	}

	defer func() {

		tableSize, err := computeTableSizes(fmt.Sprintf("users_%d", i))

		if err != nil {
			logf("could not compute table size: %v", err)
		}

		logf("created copy #%d with %s in %s.\n", i, humanize.Bytes(tableSize), time.Since(startTime))
		<-limiter
		wg.Done()
	}()

	logf("... creating copy #%d\n", i)
	var sqlCmd = fmt.Sprintf("CREATE TABLE users_%d AS (SELECT * FROM users);", i)

	_, err := db.Exec(sqlCmd)

	if err != nil {
		panic(err)
	}

	for _, sql := range cmdList {

		var sqlCmd = strings.Replace(sql, "users", fmt.Sprintf("users_%d", i), -1)

		_, err = db.Exec(sqlCmd)

		if err != nil {
			panic(err)
		}
	}
}
