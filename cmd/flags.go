package main

import (
	"fmt"
	"os"
	"os/user"
	"syscall"

	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
	"golang.org/x/crypto/ssh/terminal"
)

var (
	limiter            chan (int)
	maxRows            int
	maxJobs            int
	maxCopies          int
	scale              int
	maintenanceWorkMem string
	targetSchema       string
	debug              bool
	showVersion              bool

	dbUser        string
	dbName        string
	dbHost        string
	dbPort        int
	dbAskPassword bool
	dbPassword    string
	dbInserts     bool
)

func init() {

	user, err := user.Current()
	if err != nil {
		panic(err)
	}

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "%s generates fake data on your PostgreSQL database.\n\n", Name)
		fmt.Fprintf(os.Stderr, "Usage:\n")
		fmt.Fprintf(os.Stderr, "  %s [OPTIONS]...\n\n", Name)
		fmt.Fprintf(os.Stderr, "General Options:\n")

		flag.PrintDefaults()
		fmt.Fprintf(os.Stderr, "\nReport BUGs on https://gitlab.com/ongresinc/labs/fake-data.\n\n")
	}

	flag.IntVar(&maxRows, "maxRows", 100, "number of Rows per transaction")
	flag.IntVar(&scale, "scale", 100, "Scale factor")
	flag.IntVar(&maxJobs, "jobs", 10, "parallel jobs")
	flag.IntVar(&maxCopies, "maxCopies", 10, "number of copies of the original table")
	flag.StringVar(&maintenanceWorkMem, "maintenanceWorkMem", "2GB", "default value for 'maintenance_work_mem' GUC")
	flag.StringVar(&targetSchema, "targetSchema", "fake_data", "default schema to create the tables")

	flag.StringVarP(&dbUser, "username", "U", user.Username, "database user name")
	flag.StringVarP(&dbHost, "host", "h", "local socket", "database server host or socket directory")
	flag.StringVarP(&dbName, "dbname", "d", user.Username, "database name to connect to")
	flag.IntVarP(&dbPort, "port", "p", 5432, "database server port")

	flag.BoolVar(&debug, "debug", false, "show SQL code")
	flag.BoolVarP(&showVersion, "version","V", false, "output version information, then exit")
	flag.BoolVar(&dbInserts, "inserts", false, "create data with INSERT commands, rather than COPY")
	flag.BoolVarP(&dbAskPassword, "password", "W", false, "force password prompt")

	flag.Parse()

	if showVersion {
		fmt.Printf("%s %s\n", Name, Version)
		os.Exit(0)
	}

	viper.BindPFlags(flag.CommandLine)

	if maxJobs > scale {
		maxJobs = scale
	}

	limiter = make(chan int, maxJobs)

	if dbAskPassword {
		fmt.Printf("Password: ")
		out, err := terminal.ReadPassword(int(syscall.Stdin))

		if err != nil {
			panic(err)
		}

		dbPassword = fmt.Sprintf("%s", out)
	}
}
