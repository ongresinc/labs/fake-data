package main

import "time"

var sizesOverall uint64

func computeTableSizes(tableName string) (uint64, error) {
	var out struct {
		Size uint64
	}

	_, err := db.QueryOne(&out, `SELECT pg_total_relation_size(?) as size;`, tableName)

	sizesOverall += out.Size

	return out.Size, err
}

func computesBytesPerMin(start time.Time, totalBytes uint64) uint64 {
	metric := float64(sizesOverall) / time.Since(start).Minutes()
	return uint64(metric)
}
