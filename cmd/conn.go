package main

import (
	"fmt"

	pg "github.com/go-pg/pg/v9"
)

var db *pg.DB

func buildConn() *pg.Options {

	connOpts := &pg.Options{
		User:            dbUser,
		Database:        dbName,
		PoolSize:        maxJobs,
		ApplicationName: Name,
		OnConnect: func(conn *pg.Conn) error {
			_, err := conn.Exec("SET search_path=?;", targetSchema)
			if err != nil {
				panic(err)
			}

			_, err = conn.Exec(fmt.Sprintf("SET maintenance_work_mem TO '%s'", maintenanceWorkMem))

			if err != nil {
				panic(err)
			}

			return nil
		},
	}

	if dbPassword != "" {
		connOpts.Password = dbPassword
	}

	if dbHost != "local socket" {
		connOpts.Network = "tcp"
		connOpts.Addr = fmt.Sprintf("%s:%d", dbHost, dbPort)
		return connOpts
	}

	connOpts.Network = "unix"

	return connOpts
}
