package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func handleAbort() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println("\r- Ctrl+C pressed in Terminal")

		log.Printf("Closing all connections before exit...")

		err := db.Close()

		if err != nil {
			log.Fatalf("could not close the connections: %v", err)
		}

		log.Println("Done!")

		os.Exit(0)
	}()
}

func handleExit() {
	if e := recover(); e != nil {
		log.Fatalf("aborting due a unhandled error: %v", e)
	}
}
