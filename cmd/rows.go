package main

import (
	"bytes"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/brianvoe/gofakeit"
	"github.com/dustin/go-humanize"
	pg "github.com/go-pg/pg/v9"
	"github.com/gocarina/gocsv"
	"github.com/google/uuid"
)

var totalRows int

func createRows(db *pg.DB, maxRows int, wg *sync.WaitGroup, jobID int, startTime time.Time) {

	defer func() {
		log.Printf("[job %05d/%d (%03d%%)] saved %s rows in %s.", jobID, scale, (jobID * 100.0 / scale), humanize.Comma(int64(maxRows)), time.Since(startTime))
		<-limiter
		wg.Done()
	}()
	var allUsers []*User

	for i := 0; i < maxRows; i++ {

		id := uuid.New()

		allUsers = append(allUsers, &User{
			ID:       fmt.Sprintf("%s", id),
			Name:     gofakeit.Username(),
			Email:    gofakeit.Email(),
			BirthDay: gofakeit.Date(),
			Salary:   gofakeit.Float64Range(100.0, 10000.0),
			Phrase:   gofakeit.HackerPhrase(),
			Company:  gofakeit.Company(),
			Title:    gofakeit.JobTitle(),
		})
	}

	// log.Printf("[job-%d]... generated %d rows.", jobID, maxRows)

	err := saveRows(db, allUsers)

	if err != nil {
		panic(err)
	}

	return
}

func saveRows(db *pg.DB, allUsers []*User) error {

	if dbInserts {
		return db.Insert(&allUsers)
	}

	r := bytes.NewBufferString("")
	err := gocsv.Marshal(allUsers, r)

	if err != nil {
		panic(err)
	}

	_, err = db.CopyFrom(r, `COPY users FROM STDIN WITH CSV HEADER`)

	return err
}
