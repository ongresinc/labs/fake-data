package main

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/dustin/go-humanize"
	pg "github.com/go-pg/pg/v9"
)

const Name = "fake-data"

var (
	Commit  = "Unknow"
	Tag     = "development"
	Version = fmt.Sprintf("%s (%s)", Tag, Commit)

	startTime time.Time
)

type dbLogger struct{}

func (d dbLogger) BeforeQuery(c context.Context, q *pg.QueryEvent) (context.Context, error) {
	return c, nil
}

func (d dbLogger) AfterQuery(c context.Context, q *pg.QueryEvent) error {
	fmt.Println(q.FormattedQuery())
	return nil
}

func init() {
	handleAbort()
}

func main() {
	defer handleExit()

	log.Printf("%s (%s) - Starting...\n", Name, Version)

	startTime = time.Now()

	db = pg.Connect(buildConn())
	defer db.Close()

	if debug {
		db.AddQueryHook(dbLogger{})
	}

	err := createSchema(db)
	if err != nil {
		panic(err)
	}

	var wg sync.WaitGroup

	for i := 1; i <= scale; i++ {
		limiter <- 1

		wg.Add(1)
		go createRows(db, maxRows, &wg, i, time.Now())
	}

	wg.Wait()

	var cmdList = []string{
		"ALTER TABLE users ADD PRIMARY KEY (ID);",
		"CREATE INDEX users_name ON users (name);",
		"CREATE INDEX users_email ON users (email);",
		"CREATE INDEX users_birthday ON users (birth_day);",
		"CREATE INDEX users_salary ON users (salary);",
		"CREATE INDEX users_company ON users (company);",
		"CREATE INDEX users_company_salary ON users (company, salary);",
		"CREATE INDEX users_title ON users (title);",
	}

	log.Println("Creating table indexes...")
	for _, sql := range cmdList {
		_, err = db.Exec(sql)

		if err != nil {
			panic(err)
		}
	}

	tableSize, err := computeTableSizes("users")

	if err != nil {
		log.Printf("could not compute table size: %v", err)
	}

	log.Printf("Generated %s in %s.", humanize.Bytes(tableSize), time.Since(startTime))

	log.Println("Creating copies...")

	for i := 1; i < maxCopies; i++ {
		limiter <- 1
		wg.Add(1)
		go createCopy(i, db, &wg, cmdList, time.Now())
	}

	wg.Wait()

	endTime := time.Since(startTime)
	metric := computesBytesPerMin(startTime, sizesOverall)
	log.Printf("Generated %s over %d tables in %s (%s/m).", humanize.Bytes(sizesOverall), maxCopies+1, endTime, humanize.Bytes(metric))

	log.Println("Done!")
}
