CURRENT_COMMIT=$(shell git log -n 1 --format="%H")
CURRENT_TAG=$(shell git describe --abbrev=0 --tags)

build:
	@echo ">> building..."
	@mkdir -p dist
	@go build -o dist/fake-data -ldflags "-X main.Commit=$(CURRENT_COMMIT) -X main.Tag=$(CURRENT_TAG)" cmd/*.go

tag:
	@echo ">> creating new '$(NEW_TAG)' tag..."
	@git tag "v$(NEW_TAG)"
	@git push --tags